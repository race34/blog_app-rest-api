-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 30, 2018 at 03:52 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created`, `modified`) VALUES
(7, 'sample 5', 'blog', '2018-04-28 00:00:00', '2018-04-28 00:00:00'),
(8, 'sample 6', 'blog', '2018-04-28 00:00:00', '2018-04-28 00:00:00'),
(9, 'sample 2', 'blog', '2018-04-28 00:00:00', '2018-04-28 00:00:00'),
(17, 'sd', 'dasdasdasdasd', '2018-04-28 09:22:32', '2018-04-28 09:22:32'),
(16, 'fggg', 'gasdasdasd', '2018-04-28 09:20:25', '2018-04-28 09:20:25'),
(10, 'samafkapfa', 'adadada', '2018-04-28 06:05:59', '2018-04-28 06:05:59'),
(18, 'fggg23', 'gasdasdasd', '2018-04-28 10:02:53', '2018-04-28 10:02:53'),
(12, 'asdzxczxczxc', 'zxczxczxczxczxc', '2018-04-28 07:45:53', '2018-04-28 07:45:53'),
(13, 'asdasdasdxx', 'zxczxczxcxzc', '2018-04-28 07:49:44', '2018-04-28 07:49:44'),
(14, 'ginger', 'asdasdasdasd', '2018-04-28 07:50:04', '2018-04-28 07:50:04');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
