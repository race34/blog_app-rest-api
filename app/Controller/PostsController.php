<?php 

	App::uses('AppController','Controller');

	class PostsController extends AppController
	{

		//public $helpers	= array('Html','Form');

		public function beforeFilter(){
			parent::beforeFilter();

			//$this->response->header('Access-Control-Allow-Origin','*');
			// $this->response->header( array(
			// 	'Access-Control-Allow-Origin: *',
			// 	'Content-type: application/json'
			// ));

			$this->response->header('Access-Control-Allow-Origin','*');
			$this->response->header('Access-Control-Allow-Methods','*');
			//$this->response->header('Access-Control-Allow-Headers','Content-Type');
	        //$this->response->header('Access-Control-Allow-Type','application/json');
	        $this->response->header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
	        //$this->Security->csrfCheck = false;
			
		}

		public function index()
		{	
			$this->layout = false;

			if ($this->request->is('get','ajax')) {

				$newArray = [];

				foreach ($this->Post->find('all') as $value) {
					foreach ($value as $val) {
						$newArray[] = $val;
					}
				}

				echo json_encode($newArray);
			}
		}	

		public function add()
		{		
				$this->layout = false;
				
				$this->Post->create();

				//$this->Post->set($this->request->data);

				$response = '';

				//if ($this->Post->validates()) {
					$this->Post->save($this->request->data);

					$response = 'Successfully Added';
					$this->response->statusCode(200);
				// } else {
				// 	$response = $this->Post->validationErrors;
				// 	$this->response->statusCode(400);
				// }
			
				echo json_encode(['message' => $response]);
		}

		public function view($id)
		{
			$this->layout = false;

			$response = 'Bad Request!';
			$this->response->statusCode(400);
		
			if ($this->request->is('get','ajax')) {

				$newArray = [];

				$post = $this->Post->findById($id);

				if (!empty($post)) {
					
					$this->response->statusCode(200);

					foreach ($post as $value) {
						foreach ($value as $val) {
							$newArray[] = $val;
						}
					}

					$response = $newArray;
				} else {
					$response = 'No Post!';
					$this->response->statusCode(404);
				}
				
			}

			echo json_encode(['message' => $response]);
		}

		public function edit($id)
		{
			$this->layout = false;

			$response = '';

			if ($this->request->is('put','ajax')) {
				$this->Post->read(null, $id);
				$this->Post->set($this->request->data);

				if ($this->Post->save()) {
					$response = "Successfully Saved!";
				} else {
					$response = "Unable to update!";
				}

				// $fetchData = $this->Post->findById($id);

				// $this->Post->id = $id;

				// //$this->request->data['Post']['modified'] = date('Y-m-d H:i:s');

				// if ($this->Post->updateAll($this->request->data, ['id' => $id])) {
				// 	$response = 'Successfully Updated!';

				// } else {
				// 	$response = 'Unable to update data!';
				// }
			}

			echo json_encode(['message' => $response]);
		}

		public function delete($id)
		{	
			$this->layout = false;

			$response = '';

			if ($this->request->is('delete','ajax')) {

				if ($this->Post->delete($id)) {
					$response = 'Data successfully deleted!';
				} else {
					$response = 'Data unable to delete!';
				}
			}

			echo json_encode(['message' => $response]);
		}
	}