<?php 

	App::uses('AppModel', 'Model');

	class Post extends AppModel
	{
		public $validate = array(
			'title' => array(
				'rule' => 'notBlank',
				'required' => true,
				'message' => 'Title is required'
			),
			'body' => array(
				'rule' => 'notBlank',
				'required' => true
			)
		);
	}